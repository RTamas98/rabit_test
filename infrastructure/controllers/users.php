<!DOCTYPE html>
<html lang="hu">
<head>
    <title>RabIT test</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="../../styles/style.css"/>
</head>
<header>
    <!-- nav bar -->
    <div class="main__menu">
        <nav>
            <ul>
                <li class="main__menu--li">
                    <a href="./advertisements.php">Advertisements</a>
                </li>
                <li class="main__menu--li">
                    <a href="./main.php">Main</a>
                </li>
            </ul>
        </nav>
    </div>
</header>
<body>
<!-- users table -->
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
    </tr>
    <!-- read data from users -->
    <?php
    $conn = mysqli_connect("localhost", "root", "", "rabit");
    $sql = "SELECT * FROM users";
    $result = $conn-> query($sql);

    if($result -> num_rows > 0){
        while ($row = $result -> fetch_assoc()){
            echo "<tr><td>" . $row["id"] . "</td><td>" . $row["name"] . "</td></tr>";
        }
    }
    ?>
</table>
</body>
</html>